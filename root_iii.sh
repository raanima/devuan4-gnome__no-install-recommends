#!/usr/bin/env bash
## configure and install minimal gnome desktop environment

apt-get update
apt install doas \

cp doas.conf /etc/
chown -c root:root /etc/doas.conf
chmod -c 0400 /etc/doas.conf

adduser admin
addgroup wheel && usermod -a -G wheel admin
chage --expiredate -1 root
passwd --lock root
cd ..
rm -Rfv devuan4-gnome__no-install-recommends
ls
echo logout